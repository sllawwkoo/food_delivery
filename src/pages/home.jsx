import { ProductList } from "../components/product-list";

export function Home(props) {

	const { data, loading, error, toggleModal, addToBasket,
		isModal, basketCards, selectedCard, selectedStore } = props

	const errorMessage = error ? <h2>An error has occurred</h2> : null;
	return (
		<>
			<div className="cards__body">
				{loading ? (
					<div>loading data ...</div>
				) : (
					<>
						{errorMessage}
						<ProductList data={data}
							toggleModal={toggleModal}
							addToBasket={addToBasket}
							selectedStore={selectedStore}
							isModal={isModal}
							basketCards={basketCards}
							selectedCard={selectedCard}
						/>
					</>
				)}
			</div>
		</>
	)
}