import { ProductList } from "../components/product-list";
import { Form } from "../components/form";

export function Cart(props) {

	const { basketCards, removeFromBasket, toggleModal, isModal, selectedCard,
		incrementCount, decrementCount, totalAmount, setBasketCards, orders, setOrders } = props;
	let isCart = true;
	return (
		<>
			<h1 className="cards__title">Кошик</h1>
			{basketCards.length === 0 ? null : <Form basketCards={basketCards} totalAmount={totalAmount} setBasketCards={setBasketCards}
				                                      orders={orders} setOrders={setOrders} />}
			<div className={!isCart ? "cards__body" : "basket-cards__body"}>
				{isCart && basketCards.length !== 0 ? (
					<div className="basket-cards__total-amount">Загальна сума замовлення: <span> {totalAmount} ₴</span></div>
				) : null}
				{basketCards.length === 0 ? (
					<p className="cards__none">Ваш кошик порожній</p>
				) : (
					<ProductList
						removeFromBasket={removeFromBasket}
						isCart={true}
						basketCards={basketCards}
						toggleModal={toggleModal}
						isModal={isModal}
						selectedCard={selectedCard}
						totalAmount={totalAmount}
						incrementCount={incrementCount}
						decrementCount={decrementCount}
					/>
				)
				}
			</div>
		</>
	);
}