import { useState } from 'react';
import '../components/product-card/style.scss';

export function History(props) {
	const { orders } = props;

	const [searchEmail, setSearchEmail] = useState('');
	const [searchPhone, setSearchPhone] = useState('');

	const handleEmailSearch = (event) => {
		setSearchEmail(event.target.value);
	};

	const handlePhoneSearch = (event) => {
		const formattedPhone = formatPhoneNumber(event.target.value);
		setSearchPhone(formattedPhone);
	 };
  
	 // Функція для форматування телефонного номеру
	 const formatPhoneNumber = (phoneNumber) => {
		// Видаляємо всі символи, крім цифр
		const cleaned = phoneNumber.replace(/\D/g, '');
  
		// Додаємо формат "(111)111-11-11"
		const match = cleaned.match(/^(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})$/);
  let formatted = '';
  
  if (match) {
    formatted = '(' + match[1] + ')' + match[2] + (match[3] ? '-' + match[3] : '') + (match[4] ? '-' + match[4] : '');
  }
  
  return formatted
	 };

	// Фільтрація масиву orders за електронною поштою та номером телефону
	const filteredOrders = orders.filter((order) => {
		if (searchEmail && searchPhone) {
		  return order.email === searchEmail && order.phoneNumber === searchPhone;
		} else if (searchEmail) {
		  return order.email === searchEmail;
		} else if (searchPhone) {
		  return order.phoneNumber === searchPhone;
		} else {
		  return false;
		}
	 });
  

	return (
		<div className='cards__history history'>
			<h3 className="history__title">Історія замовлень</h3>
			<div className='history__find'>
				<label htmlFor="">Пошук за емейлом:
					<input type="text" value={searchEmail} onChange={handleEmailSearch} placeholder="email@email.com" />
				</label>
				<label htmlFor="">Пошук за номером телефону:
					<input type="text" value={searchPhone} onChange={handlePhoneSearch} placeholder="(***)***-**-**" />
				</label>
			</div>
			<ul className='history__list'>
				{filteredOrders ? (filteredOrders.map((order) => (
					<li className='history__item item-history' key={order.date}>
						<div className='item-history__element'>Ім'я: <span>{order.name}</span></div>
						<div className='item-history__element'>Емейл: <span>{order.email}</span></div>
						<div className='item-history__element'>Телефон: <span>{order.phoneNumber}</span></div>
						<div className='item-history__element'>Дата: <span>{order.date.split(",")[0]}</span></div>
						<div className='item-history__cards'>
							{order.items.map((card) => (
								<div className='history-card' key={card.id}>
									<div className="history-card__img">
										<img src={card.url} alt={card.name} />
									</div>
									<div className='history-card__content'>
										{card.name} {card.count} шт. {card.totalPrice} ₴
									</div>
								</div>
							))}
						</div>
						<div className='item-history__total'>Разом: {order.totalAmount} ₴</div>
					</li>
				))) : null}
			</ul>
		</div>
	);

}