import './App.scss';
import { Routes, Route, useLocation } from 'react-router-dom';
import { Home, Cart, History } from "./pages";
import { Header } from './components/header';
import { Sidebar } from './components/sidebar';
import { Footer } from './components/footer';
import { useState, useEffect } from 'react';
import { useFetch, useGetDataFromLS, useInitialTotalAmount } from './hooks';

function App() {

	const [isModal, setIsModal] = useState(false);
	const [selectedCard, setSelectedCard] = useState(null);
	const [selectedStore, setSelectedStore] = useState("mcdonalds");
	const [firstItemActive, setFirstItemActive] = useState(true);

	let lsBasket = useGetDataFromLS("cardsBasket");
	const [basketCards, setBasketCards] = useState(lsBasket);

	let lsOrders = useGetDataFromLS("orders");
	const [orders, setOrders] = useState(lsOrders);

	let initialStateTotalAmount = useInitialTotalAmount(basketCards);
	const [totalAmount, setTotalAmount] = useState(initialStateTotalAmount);

	const { data, loading, error } = useFetch('/data.json');

	useEffect(() => {
		localStorage.setItem("cardsBasket", JSON.stringify(basketCards));
	}, [basketCards]);

	useEffect(() => {
		localStorage.setItem("orders", JSON.stringify(orders));
	}, [orders]);

	useEffect(() => {
		// Обчислюємо загальну суму замовлення
		const totalPriceSum = basketCards.reduce((sum, card) => sum + card.totalPrice, 0);
		setTotalAmount(totalPriceSum);
	}, [basketCards]);

	const addToBasket = (card) => {
		const index = basketCards.findIndex(item => item.id === card.id);

		if (index === -1) {
			const newCard = { ...card, count: 1, totalPrice: card.price };
			const newCardsBasket = [...basketCards, newCard];
			setBasketCards(newCardsBasket);
		}
	};

	const removeFromBasket = (card) => {
		const newCardsBasket = basketCards.filter(item => item.id !== card.id);
		setBasketCards(newCardsBasket);
	};

	const toggleModal = (id) => {
		if (selectedCard === id) {
			setSelectedCard(null);
			setIsModal(false);
		} else {
			setSelectedCard(id);
			setIsModal(true);
		}
	};

	const incrementCount = (id) => {
		const updatedCardsBasket = basketCards.map(card => {
			if (card.id === id) {
				const count = card.count + 1;
				const totalPrice = count * card.price;
				return { ...card, count, totalPrice };
			}
			return card;
		});
		setBasketCards(updatedCardsBasket);
	};

	const decrementCount = (id) => {
		const updatedCardsBasket = basketCards.map(card => {
			if (card.id === id && card.count > 1) {
				const count = card.count - 1;
				const totalPrice = count * card.price;
				return { ...card, count, totalPrice };
			}
			return card;
		});
		setBasketCards(updatedCardsBasket);
	};

	const getCurrentStore = (store) => {
		setSelectedStore(store);
		setFirstItemActive(false);
	}

	const location = useLocation();
	const isHome = location.pathname === '/';

	return (
		<div className='wrapper'>
			<Header basketCards={basketCards} />
			<main className='page'>

				<section className='page__cards cards'>
					<div className="cards__container container">
						{isHome ? (<Sidebar
							selectedStore={selectedStore}
							getCurrentStore={getCurrentStore}
							firstItemActive={firstItemActive} />) : null}
						<Routes>
							<Route path='/' element={<Home
								data={data}
								loading={loading}
								error={error}
								toggleModal={toggleModal}
								addToBasket={addToBasket}
								selectedStore={selectedStore}
								basketCards={basketCards}
								isModal={isModal}
								selectedCard={selectedCard}
							/>} />
							<Route path='/cart' element={<Cart
								removeFromBasket={removeFromBasket}
								basketCards={basketCards}
								setBasketCards={setBasketCards}
								toggleModal={toggleModal}
								isModal={isModal}
								selectedCard={selectedCard}
								totalAmount={totalAmount}
								incrementCount={incrementCount}
								decrementCount={decrementCount}
								orders={orders}
								setOrders={setOrders}
							/>} />
							<Route path='/history' element={<History orders={orders} />}/>
						</Routes>
					</div>
				</section>
			</main>
			<Footer />
		</div>
	);
}

export default App
