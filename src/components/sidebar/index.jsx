import './style.scss';

export function Sidebar(props) {
	const { selectedStore, getCurrentStore, firstItemActive } = props

	const storeList = ["mcdonalds", "kfc", "hata", "pizza", "sushi"];

	return (
		<aside className='sidebar'>
			<ul className='sidebar__list'>
				{storeList.map((store, index) => (
					<li className={`sidebar__item ${selectedStore === store ? 'active' : ''} ${index === 0 && firstItemActive ? 'active' : ''}`} key={store} onClick={() => getCurrentStore(store)}>
						<div className='sidebar__img'><img src={`/img/shops/${store}.png`} alt={store} /></div>
						<span className='sidebar__desc'>{store}</span>
					</li>
				))}
			</ul>
		</aside>
	);
}