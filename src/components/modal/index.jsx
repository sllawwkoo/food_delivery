import { Cross } from "../icons";
import "./style.scss";

export function Modal(props) {

	const { header, closeButton, actions, onCloseModal, name, count, price } = props;

	return (
		<div className="modal" onClick={onCloseModal}>
			<div className="modal__content" onClick={e => e.stopPropagation()}>
				<h4 className="modal__header">{header}</h4>
				{closeButton ?
					<button className="modal__xbtn" onClick={onCloseModal}>
						<Cross />
					</button>
					: null}
				<div className="modal__name">{name}</div>
				<div className="modal__text">{count} шт.</div>
				<div className="modal__text">На суму: {price} ₴</div>
				<div className="modal__btn">
					{actions}
				</div>
			</div>
		</div>
	)
}