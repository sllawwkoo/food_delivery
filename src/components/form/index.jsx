import { useFormik } from "formik";
import { Button } from "../button";
import { PatternFormat } from "react-number-format";
import { usersSchema } from "../../schemas";
import "./style.scss";

export function Form(props) {

	const { basketCards, totalAmount, setBasketCards, setOrders, orders } = props;

	const formik = useFormik({
		initialValues: {
			name: "",
			email: "",
			phoneNumber: "",
			adress: "",
		},
		validationSchema: usersSchema,
		onSubmit: (values) => {

			const order = {
				name: values.name,
				email: values.email,
				phoneNumber: values.phoneNumber,
				adress: values.adress,
				items: basketCards,
				totalAmount: totalAmount,
				date: new Date().toLocaleString(),
			};

			// Зберігаємо замовлення
			setOrders([...orders, order])

			// Очищаємо кошик
			setBasketCards([]);

			// Скидаємо форму
			formik.resetForm();
		},
	})

	return (
		<>
			<form className="form" onSubmit={formik.handleSubmit}>
				<label htmlFor="">
					<input
						type="text"
						name="name"
						id="name"
						placeholder="Ім'я"
						value={formik.values.name}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					/>
				</label>
				{formik.errors.name && formik.touched.name ? (
					<div className="form__error">{formik.errors.name}</div>) : <div className="form__error"></div>}
				<label htmlFor="">
					<input
						type="text"
						name="email"
						id="email"
						placeholder="Електронна пошта"
						value={formik.values.email}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					/>
				</label>
				{formik.errors.email && formik.touched.email ? (
					<div className="form__error">{formik.errors.email}</div>) : <div className="form__error"></div>}
				<label htmlFor="">
					<PatternFormat
						format="(###)###-##-##"
						mask="_"
						placeholder="Номер телефону"
						name="phoneNumber"
						id="phoneNumber"
						value={formik.values.phoneNumber}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					/>
				</label>
				{formik.errors.phoneNumber && formik.touched.phoneNumber ? (
					<div className="form__error">{formik.errors.phoneNumber}</div>) : <div className="form__error"></div>}
				<label htmlFor="">
					<input
						type="text"
						name="adress"
						id="adress"
						placeholder="Адреса"
						value={formik.values.adress}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					/>
				</label>
				{formik.errors.adress && formik.touched.adress ? (
					<div className="form__error">{formik.errors.adress}</div>) : <div className="form__error"></div>}

				<Button type={'submit'} className={'btn-form'} text={'Оформити замовлення'} />
			</form>
		</>
	)
}