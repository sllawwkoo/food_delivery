import "./style.scss";

export function Button(props) {
	const { backgroundColor, text, className, onClick, type } = props;

	return (
		<button type={type} className={className} style={{ backgroundColor: backgroundColor }} onClick={onClick}>
			{text}
		</button>
	)
}