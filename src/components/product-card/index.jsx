import "./style.scss";
import { Button } from "../button";
import { Modal } from "../modal";
import { Basket, Cross } from "../icons";

export function ProductCard(props) {
	const { card, toggleModal, isModal, isCart,
		isBasket, addToBasket, removeFromBasket, count,
		incrementCount, decrementCount, price } = props;

	return (
		<>
			{!isCart ? (
				<>
					<div className="card">
						<div className="card__container">
							<div className="card__img">
								<img src={card.url} alt={card.name} />
							</div>
							<div className="card__name">{card.name}</div>
							<div className="card__price">Ціна: <span>{card.price} ₴</span></div>
							{!isBasket ? (<Button text='Додати до кошика' className={'card__btn-basket'} onClick={addToBasket} />
							) : (
								<div className="card__basket"><Basket fill={"red"} width={30} height={30} /></div>
							)}
						</div>
					</div>
				</>
			) : (
				<>
					<div className="basket-card">
						<div className="basket-card__container">
							<Button text={<Cross />} className={'basket-card__btn-cross'} onClick={toggleModal} />
							<div className="basket-card__img">
								<img src={card.url} alt={card.name} />
							</div>
							<div className="basket-card__content">
								<div className="basket-card__name">{card.name}</div>
								<div className="basket-card__price"><span>{price} ₴</span></div>
								<div className="basket-card__amount amount-card">
									<Button text={"-"} className={"amount-card__btn-minus"} onClick={decrementCount} />
									<div className="amount-card__count">{count}</div>
									<Button text={"+"} className={"amount-card__btn-plus"} onClick={incrementCount} />
								</div>
							</div>
						</div>
					</div>
				</>
			)
			}

			{isModal ? (
				<Modal
					closeButton={true}
					onCloseModal={toggleModal}
					header="Ви дійсно хочете видалити з кошика?"
					name={card.name}
					count={count}
					price={price}
					actions={
						<>
							<Button
								className={'btn-ok'}
								text='Ok'
								onClick={() => {
									removeFromBasket();
									toggleModal();
								}}
							/>
							<Button
								className={'btn-cancel'}
								text='Cancel'
								onClick={toggleModal}
							/>
						</>
					} />
			) : null
			}
		</>
	)
}