import "./style.scss";
import { ProductCard } from "../product-card";

export function ProductList(props) {

	const { data, toggleModal, addToBasket, isModal,
		basketCards, selectedCard, isCart, removeFromBasket,
		selectedStore, incrementCount, decrementCount } = props;

	const currentStore = isCart ? basketCards : data[selectedStore] || [];

	return (
		<>
			{currentStore.map((card) => (<ProductCard
				key={(card.id)}
				card={card}
				toggleModal={() => toggleModal(card.id)}
				addToBasket={() => addToBasket(card)}
				removeFromBasket={() => removeFromBasket(card)}
				isModal={isModal && selectedCard === card.id}
				isBasket={basketCards.some(item => item.id === card.id)}
				basketCards={basketCards}
				isCart={isCart}
				count={card.count}
				price={card.totalPrice}
				incrementCount={() => incrementCount(card.id)}
				decrementCount={() => decrementCount(card.id)}
			/>))}
		</>
	);
}