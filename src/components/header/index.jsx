import { NavLink } from "react-router-dom";
import { Basket } from "../icons";
import './style.scss'

export function Header(props) {

	const { basketCards } = props

	return (
		<header className="header">
			<div className="header__container container">
				<NavLink to="/">
					<div className="header__logo">
						<img src="img/fd.jpg" alt="Food Delivery" />
					</div>
				</NavLink>
				<div className="header__nav">
					<NavLink to="history">
						<div className="header__history">
							<img src="img/history.png" alt="" />
						</div>
					</NavLink>
					<NavLink to="cart">
						<div className="header__basket">
							<Basket width={35} height={35} />
							<span className="span-cart"
								style={{
									backgroundColor: +basketCards.length !== 0 ? '#e31837' : null,
									padding: +basketCards.length !== 0 ? '4px 8px' : null
								}}>
								{basketCards.length !== 0 ? basketCards.length : ''}
							</span>
						</div>
					</NavLink>
				</div>
			</div>
		</header>
	)
}