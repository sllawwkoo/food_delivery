import './style.scss';

export function Footer() {
	return (
		<footer className="footer">
			<div className="footer__container container">
				<p className="footer__text">Дякуємо, що вибрали нас для своєї доставки їжі!</p>
				<p className="footer__text">Смачного!</p>
				<p className="footer__info">Food Delivery © 2023</p>
			</div>
		</footer>
	)
}