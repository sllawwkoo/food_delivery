import { useState, useEffect } from "react";

export function useFetch(url) {
	const [loading, setLoading] = useState(true);
	const [data, setData] = useState(null);
	const [error, setError] = useState(null);

	useEffect(() => {
		setLoading(true);
		fetch(url)
			.then((res) => res.json())
			.then((result) => {
				setLoading(false);
				setData(result);
			})
			.catch((err) => {
				setLoading(false);
				setError(err);
			});
	}, [url]);
	return { data, loading, error };
}
