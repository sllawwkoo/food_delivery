import { useState, useEffect } from "react";

export function useInitialTotalAmount(basketCards) {
	const [totalAmount, setTotalAmount] = useState(0);

	useEffect(() => {
		// Обчислюємо початкову загальну суму замовлення
		const initialTotalPriceSum = basketCards.reduce((sum, card) => sum + card.totalPrice, 0);
		setTotalAmount(initialTotalPriceSum);
	}, [basketCards]);

	return totalAmount;
} 